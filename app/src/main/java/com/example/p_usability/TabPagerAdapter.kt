package com.example.p_usability

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import com.example.p_usability.fragments.*
import androidx.fragment.app.FragmentManager

class TabPagerAdapter(fm: FragmentManager, private var tabCount: Int) :
    FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {

        when (position) {
            0 -> return Home()
            1 -> return Search()
            2 -> return Card()
            3 -> return Profile()
            else -> return More()
        }
    }

    override fun getCount(): Int {
        return tabCount
    }
}